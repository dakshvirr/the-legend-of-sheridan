﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GameStates { START, PLAY, PAUSE, GAMEOVER }

public class LevelManager : MonoBehaviour
{
    static LevelManager mInstance;

    public GameStates mCurrentGameState = GameStates.START;

    [SerializeField]
    GameObject mLoadLevelCanvas;
    [SerializeField]
    Text mLoadingText;
    [SerializeField]
    Slider mProgressBar;
    [SerializeField]
    GameObject mMessageText;

    void Awake()
    {
        if (mInstance == null)
        {
            mInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        PersistentDataHandler.Instance().SetPlayerData(new PlayerData(0, 100));
    }

    public static LevelManager Instance()
    {
        return mInstance;
    }

    void Update()
    {
        switch(mCurrentGameState)
        {
            case GameStates.GAMEOVER:
                if (AudioManager.IsSoundPlaying("GameMusic"))
                {
                    AudioManager.StopSound("GameMusic");
                }
                if(AudioManager.IsSoundPlaying("MenuMusic"))
                {
                    AudioManager.StopSound("MenuMusic");
                }
                break;
            case GameStates.PLAY:
                if (AudioManager.IsSoundPlaying("MenuMusic"))
                {
                    AudioManager.StopSound("MenuMusic");
                }
                if(!AudioManager.IsSoundPlaying("GameMusic"))
                {
                    AudioManager.MusicPlay("GameMusic", true);
                }
                break;
            case GameStates.START:
            case GameStates.PAUSE:
                if (AudioManager.IsSoundPlaying("GameMusic"))
                {
                    AudioManager.StopSound("GameMusic");
                }
                if (!AudioManager.IsSoundPlaying("MenuMusic"))
                {
                    AudioManager.MusicPlay("MenuMusic", true);
                }
                break;
        }
    }

    public void LoadLevelAsync(string pCurrentLevelName, string pLoadLevelName)
    {
        mLoadLevelCanvas.SetActive(true);
        StartCoroutine(LoadAsync(pLoadLevelName));
        SceneManager.UnloadSceneAsync(pCurrentLevelName);
    }

    IEnumerator LoadAsync(string pLevelName)
    {
        yield return null;

        AsyncOperation aAsyncOperation = SceneManager.LoadSceneAsync(pLevelName);
        aAsyncOperation.allowSceneActivation = false;

        while (!aAsyncOperation.isDone)
        {
            // [0, 0.9] > [0, 1]
            float aProgress = Mathf.Clamp01(aAsyncOperation.progress / 0.9f);
            mProgressBar.value = aProgress;
            mLoadingText.text = (aProgress * 100) + "%";
            //Debug.Log("Loading progress: " + (aProgress * 100) + "%");

            // Loading completed
            if (aAsyncOperation.progress == 0.9f)
            {
                //Debug.Log("Press a key to start");
                
                mMessageText.SetActive(true);
                if (InputManager.IsStartButtonPressed())
                {
                    aAsyncOperation.allowSceneActivation = true;

                    mLoadLevelCanvas.SetActive(false);
                    mMessageText.SetActive(false);
                }
            }

            yield return null;
        }
    }

}
