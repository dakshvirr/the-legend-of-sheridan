﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PlayerData
{
    public int mScore;
    public float mHealth;
    public List<WeaponType> mWeaponsPicked;
    public bool mBroom;
    public bool mKeyCollected;
    public bool mScrollCollected;
    public PlayerData(int pScore, float pHealth)
    {
        mScore = pScore;
        mHealth = pHealth;
        mWeaponsPicked = new List<WeaponType>();
        mBroom = false;
        mKeyCollected = false;
        mScrollCollected = false;
    }

}

public class PersistentDataHandler
{
    static PersistentDataHandler mInstance;

    public static PersistentDataHandler Instance()
    {
        if(mInstance == null)
        {
            mInstance = new PersistentDataHandler();
        }
        return mInstance;
    }

    PlayerData mPlayerData = new PlayerData(0,60);
    Player mCurrentPlayer = null;

    public PlayerData GetPlayerData()
    {
        return mPlayerData;
    }

    public void SetPlayerData(PlayerData pPlayerData)
    {
        mPlayerData = pPlayerData;
    }

    public Vector3 GetPlayerPosition()
    {
        if(mCurrentPlayer == null)
        {
            return Vector3.zero;
        }
        return mCurrentPlayer.transform.position;
    }

    public void SetCurrentPlayer(Player pPlayer)
    {
        mCurrentPlayer = pPlayer;
        if(mCurrentPlayer != null)
        {
            LevelManager.Instance().mCurrentGameState = GameStates.PLAY;
        }
    }
}
