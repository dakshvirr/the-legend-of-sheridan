﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    static InputManager mInstance;
    void Awake()
    {
        if(mInstance == null)
        {
            mInstance = this;
            DontDestroyOnLoad(mInstance);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    float mRightStickYVal;
    float mRightStickXVal;
    float mLeftStickYVal;
    float mLeftStickXVal;
	
    bool mJumpButtonPressed = false;
    bool mRunButtonPressed = false;
    bool mBroomPressed = false;
    bool mStartButtonPressed = false;
    bool mBackButtonPressed = false;

    bool mIsCrouching = false;
    bool mInit = true;

    bool mDoubleAxePressed = false;
    bool mMeeleePressed = false;
    bool mHookPressed = false;

    bool mDropWeapon = false;

    void Update()
    {
        if(!mInit)
        {
            return;
        }
        RightAnalogUpdate();
        LeftAnalagUpdate();
        JumpButtonUpdate();
        RunButtonUpdate();
        CrouchUpdate();
        DoubleAxeUpdate();
        StartUpdate();
        BackUpdate();
        MeeleeUpdate();
        HookUpdate();
        BroomUpdate();
        DropUpdate();
    }

    void RightAnalogUpdate()
    {
        mRightStickXVal = Input.GetAxis("HorizontalRight");
        mRightStickYVal = Input.GetAxis("VerticalRight");
    }

    void LeftAnalagUpdate()
    {
        mLeftStickYVal = Input.GetAxis("Vertical");
        mLeftStickXVal = Input.GetAxis("Horizontal");
    }

    void JumpButtonUpdate()
    {
        mJumpButtonPressed = Input.GetButton("Jump");
    }

    void RunButtonUpdate()
    {
        mRunButtonPressed = Input.GetAxis("Run") > 0.1f;
    }

    void BroomUpdate()
    {
        mBroomPressed = Input.GetAxis("Broom") > 0.1f;
    }

    void DropUpdate()
    {
        mDropWeapon = Input.GetButton("Drop");
    }

    void CrouchUpdate()
    {
        mIsCrouching = Input.GetButton("Crouch");
    }

    void DoubleAxeUpdate()
    {
        mDoubleAxePressed = Input.GetButton("Fire1");
    }

    void StartUpdate()
    {
        mStartButtonPressed = Input.GetButton("Start");
    }

    void BackUpdate()
    {
        mBackButtonPressed = Input.GetButton("Back");
    }

    void MeeleeUpdate()
    {
        mMeeleePressed = Input.GetButton("Fire3");
    }

    void HookUpdate()
    {
        mHookPressed = Input.GetButton("Fire2");
    }

    public static bool IsDropPressed()
    {
        return mInstance.mDropWeapon;
    }

    public static float GetRightY()
    {
        return mInstance.mRightStickYVal;
    }
    public static float GetRightX()
    {
        return mInstance.mRightStickXVal;
    }

    public static float GetLeftY()
    {
        return mInstance.mLeftStickYVal;
    }

    public static float GetLeftX()
    {
        return mInstance.mLeftStickXVal;
    }

    public static bool IsJumpButtonPressed()
    {
        return mInstance.mJumpButtonPressed;
    }
    public static bool IsRunButtonPressed()
    {
        return mInstance.mRunButtonPressed;
    }

    public static bool IsDoubleAxePressed()
    {
        return mInstance.mDoubleAxePressed;
    }

    public static bool IsStartButtonPressed()
    {
        return mInstance.mStartButtonPressed;
    }

    public static bool IsBackButtonPressed()
    {
        return mInstance.mBackButtonPressed;
    }

    public static bool IsCrouching()
    {
        return mInstance.mIsCrouching;
    }

    public static bool IsMeeleePressed()
    {
        return mInstance.mMeeleePressed;
    }

    public static bool IsHookPressed()
    {
        return mInstance.mHookPressed;
    }

    public static bool IsBroomPressed()
    {
        return mInstance.mBroomPressed;
    }

    public static void StopInput()
    {
        mInstance.mInit = false;
    }
}
