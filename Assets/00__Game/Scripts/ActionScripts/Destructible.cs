﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public GameObject mDestroyParticle;
    float mDestroyTime = 1.5f;
    ParticleSystem mParticle;
    [HideInInspector]
    public bool mDestroy = false;

    void Start()
    {
        mParticle = mDestroyParticle.GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if(mDestroy)
        {
            mDestroyTime -= Time.deltaTime;
            if (mDestroyTime <= 0.0f && !mDestroyParticle.activeInHierarchy)
            {
                mDestroyParticle.SetActive(true);
            }
            else if(mDestroyParticle.activeInHierarchy)
            {
                if (mParticle.isStopped)
                {
                    Destroy(gameObject);
                }
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.GetComponent<Player>() != null && !mDestroy)
        {
            if(!AudioManager.IsSoundPlaying("BlockedDestructible"))
            {
                AudioManager.PlaySFX("BlockedDestructible");
            }
        }
    }

}
