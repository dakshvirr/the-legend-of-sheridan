﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{
    DoubleAxe,
    OneSword,
    GrappleHook,
    None
}

public struct Weapon
{
    public Weapons mWeaponObject;
    public bool mPickedUp;
    public float mKh;
    public WeaponState mState;
    public float mAttackingTime;
    public Weapon(bool pPickedUp, Weapons pWeaponObject)
    {
        mPickedUp = pPickedUp;
        mWeaponObject = pWeaponObject;
        mAttackingTime = 0.0f;
        mKh = 0.0f;
        mState = WeaponState.None;
    }
}

public enum WeaponState
{
    None,
    PickedUp,
    Holster,
    Attack,
    AttackReturn
}

public class WeaponPicker : MonoBehaviour
{
    public Dictionary<WeaponType, Weapon> mAllowedWeapons = null;

    public Transform mWeaponAnchor;
    public Transform mHolsterAnchor;

    public bool IsWeaponPicked(WeaponType pType)
    {
        return mAllowedWeapons != null && mAllowedWeapons[pType].mPickedUp;
    }

    public bool PickWeapon(WeaponType pType, Animator pAnimator)
    {
        if(pType == WeaponType.None)
        {
            return false;
        }
        if (mAllowedWeapons == null)
        {
            return false;
        }
        if(mAllowedWeapons[pType].mWeaponObject == null)
        {
            return false;
        }
        if(mAllowedWeapons[pType].mPickedUp)
        {
            return false;
        }
        pAnimator.SetInteger("ItemId", (int) pType);
        pAnimator.SetTrigger("PickUp");
        SetUpWeapon(null, pType);
        return true;
    }

    public bool AttackWeapon(WeaponType pType, Animator pAnimator)
    {
        if (pType == WeaponType.None)
        {
            pAnimator.SetInteger("WeaponId", -1);
            pAnimator.SetInteger("ActionId", 2);
            pAnimator.SetTrigger("ActionReaction");
            return true;
        }
        if (mAllowedWeapons == null)
        {
            return false;
        }
        if (mAllowedWeapons[pType].mWeaponObject == null)
        {
            return false;
        }
        if (!mAllowedWeapons[pType].mPickedUp)
        {
            return false;
        }
        if(mAllowedWeapons[pType].mState == WeaponState.Attack)
        {
            return false;
        }
        pAnimator.SetInteger("WeaponId", (int)pType);
        pAnimator.SetInteger("ActionId", 2);
        pAnimator.SetTrigger("ActionReaction");
        mAllowedWeapons[pType].mWeaponObject.FireWeapon();
        return true;
    }

    public void SetUpWeapon(Weapons pWeapon, WeaponType pType)
    {
        Weapon aWeapon = mAllowedWeapons[pType];
        aWeapon.mPickedUp = true;
        if(pWeapon != null)
        {
            aWeapon.mWeaponObject = pWeapon;
            aWeapon.mState = WeaponState.Holster;
        }
        aWeapon.mWeaponObject.RemoveTriggerable();
        aWeapon.mWeaponObject.transform.SetParent(transform);
        aWeapon.mWeaponObject.mActivePicker = this;
        mAllowedWeapons[pType] = aWeapon;
    }


    public void DropWeapon(WeaponType pType, GameObject pWeaponPrefab)
    {
        if(mAllowedWeapons[pType].mWeaponObject != null && mAllowedWeapons[pType].mPickedUp)
        {
            mAllowedWeapons[pType].mWeaponObject.mActivePicker = null;
            Destroy(mAllowedWeapons[pType].mWeaponObject.gameObject);
            Weapon aW = mAllowedWeapons[pType];
            aW.mWeaponObject = null;
            aW.mPickedUp = false;
            aW.mState = WeaponState.None;
            aW.mKh = 0.0f;
            aW.mAttackingTime = 0.0f;
            mAllowedWeapons[pType] = aW;
            PlayerData aData = PersistentDataHandler.Instance().GetPlayerData();
            aData.mWeaponsPicked.Remove(pType);
            PersistentDataHandler.Instance().SetPlayerData(aData);
            Instantiate(pWeaponPrefab, gameObject.transform.position + new Vector3(Random.Range(3.0f, 6.0f), 0, Random.Range(6.0f, 3.0f)), Quaternion.Euler(0, 0, 0));
        }
    }

}
