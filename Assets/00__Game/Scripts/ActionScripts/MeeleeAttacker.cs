﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeeleeAttacker : MonoBehaviour
{
    public bool mActive = false;
    Player mPlayer = null;
    public float mDamage;
    void Start()
    {
        mPlayer = GetComponentInParent<Player>();
    }


    void OnTriggerEnter(Collider other)
    {
        if(!mActive)
        {
            return;
        }
        if(other.attachedRigidbody == mPlayer.mRigidBody)
        {
            return;
        }
        if(other.attachedRigidbody == null)
        {
            return;
        }
        Damageable aDamageable = other.attachedRigidbody.gameObject.GetComponent<Damageable>();
        if (aDamageable == null)
        {
            return;
        }

        aDamageable.Damage(mDamage, Damageable.DamagerType.WalkEnemy);
    }
}
