﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraHealth : Mixin
{
    public float mHealthBoost;

    public override void Dispatch(GameObject pObject)
    {
        Player aPlayer = pObject.GetComponent<Player>();
        if(aPlayer == null)
        {
            return;
        }
        else
        {
            PlayerData aData = PersistentDataHandler.Instance().GetPlayerData();
            aData.mHealth += mHealthBoost;
            AudioManager.PlaySFX("PickUp");
            PersistentDataHandler.Instance().SetPlayerData(aData);
            UIManager.Instance().UpdateHealth(aData.mHealth);
            Destroy(gameObject);
        }
    }

    public override void ResetDispatch(GameObject pObject)
    {
        //for future extension
    }
}
