﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapons : Mixin
{
    public WeaponType mType;
    public float mWeaponDamage;
    public Triggerable mTriggerable;
    [HideInInspector]
    public WeaponPicker mActivePicker = null;

    public override void Dispatch(GameObject pObject)
    {
        if(mActivePicker != null)
        {
            return;
        }
        Player aPlayer = pObject.GetComponent<Player>();
        if (aPlayer == null)
        {
            Enemy aEnemy = pObject.GetComponent<Enemy>();
            if (aEnemy != null)
            {
                mActivePicker = aEnemy.mPicker;
            }
        }
        else if(aPlayer.mMovementScript.GetState() == PlayerMovement.NavState.Paused)
        {
            return;
        }
        else
        {
            mActivePicker = aPlayer.mPicker;
            switch(mType)
            {
                case WeaponType.DoubleAxe: UIManager.Instance().ShowNotification("Press A to pick Axe");
                    break;
                case WeaponType.OneSword: UIManager.Instance().ShowNotification("Press X to pick Sword");
                    break;
                case WeaponType.GrappleHook: UIManager.Instance().ShowNotification("Press B to pick Grapple Hook");
                    break;
            }
        }
        if (mActivePicker != null)
        {
            if (mActivePicker.mAllowedWeapons == null)
            {
                return;
            }
            if (!mActivePicker.mAllowedWeapons.ContainsKey(mType))
            {
                return;
            }
            if (mActivePicker.mAllowedWeapons[mType].mPickedUp)
            {
                return;
            }
            if (mActivePicker.mAllowedWeapons[mType].mWeaponObject != null)
            {
                return;
            }
            Weapon aWeapon = mActivePicker.mAllowedWeapons[mType];
            aWeapon.mWeaponObject = this;
            mActivePicker.mAllowedWeapons[mType] = aWeapon;
            mTriggerable.DisableTriggerable();
        }
    }

    public override void ResetDispatch(GameObject pObject)
    {
        if(mActivePicker.transform.parent.gameObject != pObject)
        {
            return;
        }
        Player aPlayer = pObject.GetComponent<Player>();
        if (aPlayer == null)
        {
            Enemy aEnemy = pObject.GetComponent<Enemy>();
            if (aEnemy == null)
            {
                return;
            }
        }
        if(aPlayer != null)
        {
            UIManager.Instance().HideNotification();
        }
        Weapon aWeapon = mActivePicker.mAllowedWeapons[mType];
        aWeapon.mWeaponObject = null;
        mActivePicker.mAllowedWeapons[mType] = aWeapon;
        mActivePicker = null;
    }

    public abstract void FireWeapon();

    public void RemoveTriggerable()
    {
        mTriggerable.DontActivateOnExit();
        mTriggerable.gameObject.SetActive(false);
    }

}
