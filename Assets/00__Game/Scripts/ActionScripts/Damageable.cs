﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    public enum DamagerType
    {
        None,
        FlyEnemy,
        JumpEnemy,
        WalkEnemy
    }
    [HideInInspector]
    public float mCurrentDamage = -1;
    [HideInInspector]
    public DamagerType mDamagerType = DamagerType.None;
    public void Damage(float pDamage, DamagerType pType)
    {
        mCurrentDamage = pDamage;
        mDamagerType = pType;
    }
}
