﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BroomProjectile : MonoBehaviour
{
    public GameObject mRenderable;
    public float mSpeed;
    public float mDamage;
    public float mLifeTime;
    public void InstantiateProjectile()
    {
        AudioManager.PlaySFX("ShootBroomProjectile");
        mRenderable.SetActive(true);
    }

    void Update()
    {
        if(!mRenderable.activeInHierarchy)
        {
            return;
        }
        mLifeTime -= Time.deltaTime;
        if (mLifeTime <= 0.0f)
        {
            Destroy(gameObject);
        }
        transform.position += mSpeed * Time.deltaTime * transform.forward;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
        {
            Destroy(gameObject);
            return;
        }
        Damageable aDamageable = other.attachedRigidbody.gameObject.GetComponent<Damageable>();
        if (aDamageable == null)
        {
            Destroy(gameObject);
            return;
        }

        aDamageable.Damage(mDamage, Damageable.DamagerType.WalkEnemy);
    }

}
