﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIK : MonoBehaviour
{


    Player mPlayer;

    float mIkPositionWeight;
    float mIkRotationWeight;
    float mIkLookAtWeight;

    Transform mLeftHand = null;
    Transform mRightHand = null;
    Transform mLeftLeg = null;
    Transform mRightLeg = null;
    Transform mLookAt = null;

    bool mBIK = false;
    bool mAIK = false;
    bool mSIK = false;
    bool mHIK = false;
    bool mSLL = false;
    bool mGIK = false;


    void Start()
    {
        mPlayer = GetComponent<Player>();
    }

    void Update()
    {
        if (LevelManager.Instance().mCurrentGameState != GameStates.PLAY)
        {
            return;
        }
        mIkPositionWeight = mPlayer.mAnimator.GetFloat("IK Position Curve");
        mIkRotationWeight = mPlayer.mAnimator.GetFloat("IK Rotation Curve");
        mIkLookAtWeight = mPlayer.mAnimator.GetFloat("IK Look At Curve");
        BroomAnimationUpdate();
        if(!mBIK)
        {
            AxeAnimationUpdate();
            if(!mAIK)
            {
                HookAnimationUpdate();
                if (!mHIK)
                {
                    SwordAnimationUpdate();
                    if(!mSIK)
                    {
                        DoorAnimationUpdate();
                    }
                }
            }
        }
    }

    void OnAnimatorIK(int layerIndex)
    {
        if(layerIndex == 0)
        {
            if(mLeftHand != null)
            {
                mPlayer.mAnimator.SetIKPositionWeight(AvatarIKGoal.LeftHand, mIkPositionWeight);
                mPlayer.mAnimator.SetIKRotationWeight(AvatarIKGoal.LeftHand, mIkRotationWeight);
                mPlayer.mAnimator.SetIKPosition(AvatarIKGoal.LeftHand, mLeftHand.position);
                mPlayer.mAnimator.SetIKRotation(AvatarIKGoal.LeftHand, mLeftHand.rotation);
            }
            if(mRightHand != null)
            {
                mPlayer.mAnimator.SetIKPositionWeight(AvatarIKGoal.RightHand, mIkPositionWeight);
                mPlayer.mAnimator.SetIKRotationWeight(AvatarIKGoal.RightHand, mIkRotationWeight);
                mPlayer.mAnimator.SetIKPosition(AvatarIKGoal.RightHand, mRightHand.position);
                mPlayer.mAnimator.SetIKRotation(AvatarIKGoal.RightHand, mRightHand.rotation);
            }
            if(mLeftLeg != null)
            {
                mPlayer.mAnimator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, mIkPositionWeight);
                mPlayer.mAnimator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, mIkRotationWeight);
                mPlayer.mAnimator.SetIKPosition(AvatarIKGoal.LeftFoot, mLeftLeg.position);
                mPlayer.mAnimator.SetIKRotation(AvatarIKGoal.LeftFoot, mLeftLeg.rotation);
            }
            if(mRightLeg != null)
            {
                mPlayer.mAnimator.SetIKPositionWeight(AvatarIKGoal.RightFoot, mIkPositionWeight);
                mPlayer.mAnimator.SetIKRotationWeight(AvatarIKGoal.RightFoot, mIkRotationWeight);
                mPlayer.mAnimator.SetIKPosition(AvatarIKGoal.RightFoot, mRightLeg.position);
                mPlayer.mAnimator.SetIKRotation(AvatarIKGoal.RightFoot, mRightLeg.rotation);
            }
            if(mLookAt != null)
            {
                mPlayer.mAnimator.SetLookAtWeight(mIkLookAtWeight);
                mPlayer.mAnimator.SetLookAtPosition(mLookAt.position);
            }
            else
            {
                mPlayer.mAnimator.SetLookAtWeight(0);
            }
        }
    }

    void SetDoubleAxeHandles()
    {
        if(!mAIK)
        {
            mLeftHand = null;
            mRightHand = null;
        }
        else
        {
            DoubleAxe aAxe = (DoubleAxe)(mPlayer.mPicker.mAllowedWeapons[WeaponType.DoubleAxe].mWeaponObject);
            if (aAxe == null)
            {
                return;
            }
            mLeftHand = aAxe.mLeftHandHandle;
            mRightHand = aAxe.mRightHandHandle;
        }

    }

    void AxeAnimationUpdate()
    {
        if(mPlayer.mPicker.mAllowedWeapons[WeaponType.DoubleAxe].mWeaponObject == null)
        {
            mAIK = false;
            return;
        }
        if (mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabDoubleAxeSS -> GrabDoubleAxeStart"))
        {
            mAIK = true;
            mLookAt = mPlayer.mPicker.mAllowedWeapons[WeaponType.DoubleAxe].mWeaponObject.transform;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabDoubleAxeStart -> GrabDoubleAxeEnd"))
        {
            mAIK = true;
            mLookAt = null;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabDoubleAxeEnd -> KeepAxeBehindAfterGrab"))
        {
            mAIK = true;
            mLookAt = null;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("KeepAxeBehindAfterGrab -> Exit"))
        {
            mAIK = false;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("Entry -> DrawAxeBehind"))
        {
            mAIK = false;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("DrawAxeBehind -> AxeAttackPosition"))
        {
            mAIK = false;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("AxeAttackPosition -> Axe Attack"))
        {
            mAIK = false;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("Axe Attack -> Axe Attack End"))
        {
            mAIK = false;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("Axe Attack End -> Keep Axe Behind"))
        {
            mAIK = false;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("KeepAxeBehind -> Exit"))
        {
            mAIK = false;
        }
        SetDoubleAxeHandles();
    }

    void SetOneSwordHandle()
    {
        if(!mSIK)
        {
            mRightHand = null;
        }
        else
        {
            OneSword aSword = (OneSword)(mPlayer.mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject);
            if (aSword == null)
            {
                return;
            }
            mRightHand = aSword.mRightHandHandle;

        }
    }

    void SwordAnimationUpdate()
    {
        if (mPlayer.mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject == null)
        {
            return;
        }
        if (mPlayer.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("GrabOneSwordStart"))
        {
            mSIK = true;
            mLookAt = mPlayer.mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.transform;
        }
        else if (mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabOneSwordEnd -> Exit"))
        {
            mSIK = false;
            mLookAt = null;
        }
        else if(mPlayer.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            mSIK = false;
        }
        SetOneSwordHandle();
    }

    void SetBroomHandle()
    {
        if(!mBIK)
        {
            mLeftLeg = null;
            mRightLeg = null;
            mLeftHand = null;
            mRightHand = null;
        }
        else
        {
            if(mSLL)
            {
                mLeftLeg = mPlayer.mBroom.mLeftLegHandle;
            }
            mRightLeg = mPlayer.mBroom.mRightLegHandle;
            mLeftHand = mPlayer.mBroom.mLeftHandHandle;
            mRightHand = mPlayer.mBroom.mRightHandHandle;
        }
    }

    void BroomAnimationUpdate()
    {
        if(mPlayer.mBroom == null)
        {
            mBIK = false;
            return;
        }

        if(!mPlayer.mBroom.mIsEquipped)
        {
            mBIK = false;
            mSLL = false;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("OnBroom -> StayBroom"))
        {
            mBIK = true;
            mSLL = true;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("StayBroom -> OffBroom"))
        {
            mBIK = false;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("OffBroom -> Exit"))
        {
            mBIK = false;
            mSLL = false;
        }
        SetBroomHandle();
    }

    void SetHookHandle()
    {
        if (!mHIK)
        {
            mRightHand = null;
        }
        else
        {
            HookProjectile aProjectile = (HookProjectile)(mPlayer.mPicker.mAllowedWeapons[WeaponType.GrappleHook].mWeaponObject);
            if (aProjectile == null)
            {
                return;
            }
            mRightHand = aProjectile.mRightHandHandle;
        }
    }

    void HookAnimationUpdate()
    {
        if (mPlayer.mPicker.mAllowedWeapons[WeaponType.GrappleHook].mWeaponObject == null)
        {
            mHIK = false;
            return;
        }
        if (mPlayer.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("GrabProjectileStart"))
        {
            mLookAt = mPlayer.mPicker.mAllowedWeapons[WeaponType.GrappleHook].mWeaponObject.transform;
            mHIK = true;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabProjectileStart -> GrabProjectileEnd"))
        {
            mLookAt = null;
            mHIK = false;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("ProjectileThrow -> ProjectileStay"))
        {
            mHIK = true;
        }
        else if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("ProjectileStay -> ProjectileReturn"))
        {
            mHIK = false;
        }
        SetHookHandle();
    }

    void SetDoorHandle()
    {
        if(!mGIK)
        {
            return;
        }
        if(mPlayer.mActiveGate == null)
        {
            return;
        }

        mLeftHand = mPlayer.mActiveGate.mLeftHandle;
        mRightHand = mPlayer.mActiveGate.mRightHandle;
    }

    void DoorAnimationUpdate()
    {
        if(mPlayer.mActiveGate == null)
        {
            mGIK = false;
            return;
        }
        if(mPlayer.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("Open Door"))
        {
            mGIK = true;
        }
        if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("Open Door -> Exit"))
        {
            mGIK = false;
        }
        if(mPlayer.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("CantOpenStart"))
        {
            mGIK = true;
        }
        if(mPlayer.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("CantOpenTry"))
        {
            mGIK = true;
        }
        if(mPlayer.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("CantOpenEnd"))
        {
            mGIK = true;
        }
        if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("CantOpenEnd -> Exit"))
        {
            mGIK = false;
        }
        SetDoorHandle();
    }
    public void SetIK(WeaponType pType)
    {
        switch(pType)
        {
            case WeaponType.DoubleAxe:
                mAIK = true;
                break;
            case WeaponType.GrappleHook:
                mHIK = true;
                break;
            case WeaponType.OneSword:
                mSIK = true;
                break;
            default:
                mBIK = true;
                break;
        }
    }

}
