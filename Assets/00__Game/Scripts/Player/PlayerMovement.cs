﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public enum NavState
    {
        Idle,
        Walk,
        Run,
        Crouch,
        Jump,
        Paused
    }

    public Transform mWorldReference;

    NavState mPreviousState = NavState.Idle;
    NavState mCurrentState = NavState.Idle;

    Vector3 mGroundNormal = Vector3.up;
    float mGCheckDist = 0.2f;
    float mForward;
    float mTurn;

    Player mPlayer;
    [HideInInspector]
    public float mColliderHeight;
    [HideInInspector]
    public Vector3 mColliderCenter;

    void Start()
    {
        mPlayer = GetComponent<Player>();
        mColliderHeight = mPlayer.mMainBodyCollider.height;
        mColliderCenter = mPlayer.mMainBodyCollider.center;
    }

    void OnAnimatorMove()
    {
        if (Time.deltaTime > 0 && mCurrentState != NavState.Paused)
        {
            Vector3 aVelocity = (mPlayer.mAnimator.deltaPosition / Time.deltaTime);
            aVelocity.y = mPlayer.mRigidBody.velocity.y;
            mPlayer.mRigidBody.velocity = aVelocity;
        }
    }

    void Update()
    {
        if (LevelManager.Instance().mCurrentGameState != GameStates.PLAY)
        {
            mPlayer.mAnimator.SetFloat("Vertical", 0.0f);
            mPlayer.mAnimator.SetFloat("Horizontal", 0.0f);
            return;
        }
        NavUpdate();
    }

    #region Navigation
    void NavUpdate()
    {
        if(mCurrentState == NavState.Paused)
        {
            return;
        }
        if (InputManager.IsCrouching() && (mCurrentState == NavState.Walk || mCurrentState == NavState.Idle))
        {
            mPreviousState = mCurrentState;
            mCurrentState = NavState.Crouch;
        }
        if(mCurrentState != NavState.Jump)
        {
            if (InputManager.IsJumpButtonPressed() && mCurrentState != NavState.Crouch)
            {
                mPreviousState = mCurrentState;
                mCurrentState = NavState.Jump;
                if(mPreviousState == NavState.Run)
                {
                    mPlayer.mAnimator.SetBool("Flip", true);
                }
                else
                {
                    mPlayer.mAnimator.SetBool("Flip", false);
                }
                return;
            }
            CheckIsGrounded();

            Vector3 aMovementVector = InputManager.GetLeftY() * mWorldReference.forward + InputManager.GetLeftX() * mWorldReference.right;
            aMovementVector.Normalize();
            aMovementVector = transform.InverseTransformDirection(aMovementVector);
            aMovementVector = Vector3.ProjectOnPlane(aMovementVector, mGroundNormal);

            float aIdleMag = aMovementVector.magnitude;
            mPlayer.mAnimator.SetFloat("IdleMag", aIdleMag, 0.1f, Time.deltaTime);

            if (aIdleMag >= 0.1f && mCurrentState != NavState.Crouch)
            {
                if(InputManager.IsRunButtonPressed())
                {
                    mPreviousState = mCurrentState;
                    mCurrentState = NavState.Run;
                }
                else
                {
                    mPreviousState = mCurrentState;
                    mCurrentState = NavState.Walk;
                }
            }
            else if(!InputManager.IsCrouching())
            {
                mPreviousState = mCurrentState;
                mCurrentState = NavState.Idle;
            }
            mTurn = Mathf.Atan2(aMovementVector.x, aMovementVector.z);
            mForward = aMovementVector.z;

            ScaleCapsule();
            TurnExtra();
        }
        else
        {
            mPlayer.mAnimator.SetBool("Jump", true);
            JumpUpdate();
        }
        MovementUpdate();
    }

    void TurnExtra()
    {
        if(mCurrentState != NavState.Idle)
        {
            float aTurnSpeed = Mathf.Lerp(180, 360, mForward);
            transform.Rotate(0, mTurn * aTurnSpeed * Time.deltaTime, 0);
        }
    }

    void JumpUpdate()
    {
        if(mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("Land -> Exit")
        || mPlayer.mAnimator.GetAnimatorTransitionInfo(0).IsName("Standing Jump -> Idle"))
        {
            mPlayer.mAnimator.SetBool("Jump", false);
            mCurrentState = mPreviousState;
        }
    }

    void MovementUpdate()
    {
        if (mCurrentState != NavState.Jump && mCurrentState != NavState.Idle)
        {
            mPlayer.mAnimator.SetFloat("Vertical", mForward, 0.1f, Time.deltaTime);
            mPlayer.mAnimator.SetFloat("Horizontal", mTurn, 0.1f, Time.deltaTime);
            mPlayer.mAnimator.SetBool("Crouch", mCurrentState == NavState.Crouch);
            mPlayer.mAnimator.SetBool("Run", mCurrentState == NavState.Run);
        }
        else
        {
            mPlayer.mAnimator.SetFloat("Vertical", 0.0f);
            mPlayer.mAnimator.SetFloat("Horizontal", 0.0f);
            mPlayer.mAnimator.SetBool("Crouch", false);
            mPlayer.mAnimator.SetBool("Run", false);
        }
    }

    #endregion


    #region Helper Functions
    void ScaleCapsule()
    {
        if(mCurrentState == NavState.Crouch)
        {
            if(mPlayer.mMainBodyCollider.height > mColliderHeight * 0.5f)
            {
                mPlayer.mMainBodyCollider.height = mColliderHeight * 0.5f;
                mPlayer.mMainBodyCollider.center = mColliderCenter * 0.5f;
            }
        }
        else
        {
            float aHalfRadius = mPlayer.mMainBodyCollider.radius * 0.5f;
            Ray aCrouchRay = new Ray(mPlayer.mRigidBody.position + Vector3.up * aHalfRadius, Vector3.up);
            float aRayLength = mColliderHeight - aHalfRadius;
            RaycastHit aHitInfo;
            if(Physics.SphereCast(aCrouchRay, aHalfRadius, out aHitInfo, aRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                if(aHitInfo.collider.attachedRigidbody != mPlayer.mRigidBody)
                {
                    mCurrentState = NavState.Crouch;
                    return;
                }
            }
            mPlayer.mMainBodyCollider.height = mColliderHeight;
            mPlayer.mMainBodyCollider.center = mColliderCenter;
        }
    }
    void CheckIsGrounded()
    {
        RaycastHit aHitInfo;
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out aHitInfo, mGCheckDist))
        {
            mGroundNormal = aHitInfo.normal;
            //mPlayer.mAnimator.applyRootMotion = true;
        }
        else
        {
            mGroundNormal = Vector3.up;
            //mPlayer.mAnimator.applyRootMotion = false;
        }
    }
    public NavState GetState()
    {
        return mCurrentState;
    }
    public void SetState(NavState pState, bool pPrevState = false)
    {
        if(pPrevState)
        {
            mPreviousState = NavState.Idle;
        }
        else
        {
            mPreviousState = mCurrentState;
        }
        mCurrentState = pState;
    }
    #endregion

}
