﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct WeaponPrefabs
{
    public WeaponType mWeaponType;
    public GameObject mWeapon;
}

public class Player : MonoBehaviour
{
    [Header("Unity Components")]
    public Animator mAnimator;
    public Rigidbody mRigidBody;
    public CapsuleCollider mMainBodyCollider;
    public CapsuleCollider[] mShoesAndHands;
    [Header("Custom Scripts")]
    public PlayerIK mIKScript;
    public PlayerMovement mMovementScript;
    public WeaponPicker mPicker;
    public Damageable mDamageable;
    public MeeleeAttacker mMeelee;
    [Header("Prefabs For Persistence")]
    public List<WeaponPrefabs> mWeapons;
    public GameObject mBroomPrefab;
    public GameObject mKey;
    public GameObject mScroll;

    [HideInInspector]
    public Broom mBroom = null;
    [HideInInspector]
    public FenceGate mActiveGate = null;
    [HideInInspector]
    public Key mCollectedKey = null;
    [HideInInspector]
    public Scroll mCollectedScroll = null;
    void Start()
    {
        PlayerData aPlayerData = PersistentDataHandler.Instance().GetPlayerData();
        UIManager.Instance().UpdateScore(aPlayerData.mScore);
        UIManager.Instance().UpdateHealth(aPlayerData.mHealth);
        mPicker.mAllowedWeapons = new Dictionary<WeaponType, Weapon>();
        mPicker.mAllowedWeapons.Add(WeaponType.DoubleAxe, new Weapon(false, null));
        mPicker.mAllowedWeapons.Add(WeaponType.OneSword, new Weapon(false, null));
        mPicker.mAllowedWeapons.Add(WeaponType.GrappleHook, new Weapon(false, null));
        foreach (WeaponPrefabs aPrefab in mWeapons)
        {
            if(aPlayerData.mWeaponsPicked.Contains(aPrefab.mWeaponType))
            {
                GameObject aWeapon = Instantiate(aPrefab.mWeapon);
                switch(aPrefab.mWeaponType)
                {
                    case WeaponType.DoubleAxe: mPicker.SetUpWeapon(aWeapon.GetComponent<DoubleAxe>(), aPrefab.mWeaponType);
                        break;
                    case WeaponType.GrappleHook: mPicker.SetUpWeapon(aWeapon.GetComponent<HookProjectile>(), aPrefab.mWeaponType);
                        aWeapon.transform.Rotate(mPicker.transform.right, 90);
                        break;
                    case WeaponType.OneSword: mPicker.SetUpWeapon(aWeapon.GetComponent<OneSword>(), aPrefab.mWeaponType);
                        break;
                }
                Weapon aW = mPicker.mAllowedWeapons[aPrefab.mWeaponType];
                aW.mState = WeaponState.Holster;
                aW.mKh = 1.0f;
                mPicker.mAllowedWeapons[aPrefab.mWeaponType] = aW;
            }
        }
        if(aPlayerData.mBroom)
        {
            GameObject aBroom = Instantiate(mBroomPrefab);
            mBroom = aBroom.GetComponent<Broom>();
            mBroom.RemoveTriggerable();
        }
        if(aPlayerData.mKeyCollected)
        {
            GameObject aCollectedKey = Instantiate(mKey,mPicker.mWeaponAnchor.transform,false);
            mCollectedKey = aCollectedKey.GetComponent<Key>();
            mCollectedKey.RemoveTriggerable();
            aCollectedKey.SetActive(false);
        }
        if(aPlayerData.mScrollCollected)
        {
            GameObject aCollectedScroll = Instantiate(mScroll, mPicker.mWeaponAnchor.transform, false);
            mCollectedScroll = aCollectedScroll.GetComponent<Scroll>();
            mCollectedScroll.RemoveTriggerable();
            aCollectedScroll.SetActive(false);

        }
        PersistentDataHandler.Instance().SetCurrentPlayer(this);
    }

    void Update()
    {
        if(LevelManager.Instance().mCurrentGameState != GameStates.PLAY)
        {
            return;
        }
        if(mMovementScript.GetState() == PlayerMovement.NavState.Jump || mMovementScript.GetState() == PlayerMovement.NavState.Crouch)
        {
            return;
        }
        if(mMovementScript.GetState() == PlayerMovement.NavState.Paused)
        {
            ActionUpdates();
        }
        else
        {
            DropWeapons();
            DamageableUpdate();
            DoorUpdate();
            BroomUpdate();
            DoubleAxeUpdate();
            HookUpdate();
            OneSwordUpdate();
        }
    }

    void DropWeapons()
    {
        if (!InputManager.IsDropPressed())
        {
            return;
        }
        bool aPlay = false;
        foreach (WeaponPrefabs aPrefab in mWeapons)
        {
            if (mPicker.mAllowedWeapons[aPrefab.mWeaponType].mPickedUp)
            {
                mPicker.DropWeapon(aPrefab.mWeaponType, aPrefab.mWeapon);
                aPlay = true;
                UIManager.Instance().ToggleWeapon(aPrefab.mWeaponType);
            }
        }
        if (aPlay)
        {
            AudioManager.PlaySFX("DropWeapons");
        }
    }
    void DamageableUpdate()
    {
        if (mDamageable.mCurrentDamage > 0)
            {
                PlayerData aData = PersistentDataHandler.Instance().GetPlayerData();
                if (aData.mHealth > 0)
                {
                    aData.mHealth -= mDamageable.mCurrentDamage;
                    if (aData.mHealth <= 0)
                    {
                        aData.mHealth = 0;
                        Die();
                    }
                    else
                    {
                        Damage();
                    }
                    PersistentDataHandler.Instance().SetPlayerData(aData);
                    UIManager.Instance().UpdateHealth(aData.mHealth);
                }
                mDamageable.mCurrentDamage = -1;
                mDamageable.mDamagerType = Damageable.DamagerType.None;
            }
    }

    void BroomUpdate()
    {
        if (mMovementScript.GetState() == PlayerMovement.NavState.Paused)
        {
            return;
        }
        if (mBroom == null)
        {
            return;
        }
        if(!InputManager.IsBroomPressed())
        {
            return;
        }

        if(mBroom.IsTriggerable())
        {
            mBroom.RemoveTriggerable();
            PlayerData aData = PersistentDataHandler.Instance().GetPlayerData();
            aData.mBroom = true;
            PersistentDataHandler.Instance().SetPlayerData(aData);
            UIManager.Instance().HideNotification();
        }
        if (!mBroom.mIsEquipped)
        {
            EquipBroom();
            mMovementScript.SetState(PlayerMovement.NavState.Paused);
        }
    }

    void EquipBroom()
    {
        if(mBroom.mCurTime > 0.0f)
        {
            mBroom.mIsEquipped = true;
            if (!mBroom.mPickedUp)
            {
                mBroom.mPickedUp = true;
            }
            else
            {
                mBroom.transform.position = transform.position - transform.right;
                mBroom.transform.localPosition = new Vector3(mBroom.transform.localPosition.x,mBroom.mYUnmountPos, mBroom.transform.localPosition.z);
                mBroom.mRenderable.SetActive(true);
            }
            mRigidBody.isKinematic = true;
            if (mMainBodyCollider.height > mMovementScript.mColliderHeight * 0.5f)
            {
                mMainBodyCollider.height = mMovementScript.mColliderHeight * 0.5f;
                mMainBodyCollider.center = mMovementScript.mColliderCenter * 1.5f;
            }
            foreach(CapsuleCollider aShoenHand in mShoesAndHands)
            {
                aShoenHand.enabled = false;
            }
            AudioManager.PlaySFX("ClimbBroom");
            mAnimator.SetTrigger("OnBroom");
            mIKScript.SetIK(WeaponType.None);
        }
    }

    void UnEquipBroom()
    {
        mBroom.mCanMove = false;
        mBroom.mRigidBody.isKinematic = true;
        mBroom.mCollider.enabled = false;
        transform.SetParent(null);
        mAnimator.SetBool("OffBroom", true);
        AudioManager.PlaySFX("OffBroom");
    }

    void DoubleAxeUpdate()
    {
        if (mMovementScript.GetState() == PlayerMovement.NavState.Paused)
        {
            return;
        }
        if (!InputManager.IsDoubleAxePressed())
        {
            return;
        }
        if (mPicker.mAllowedWeapons[WeaponType.DoubleAxe].mWeaponObject != null)
        {
            if(mPicker.mAllowedWeapons[WeaponType.DoubleAxe].mPickedUp)
            {
                if(mPicker.AttackWeapon(WeaponType.DoubleAxe, mAnimator))
                {
                    mMovementScript.SetState(PlayerMovement.NavState.Paused);
                    AudioManager.PlaySFX("AxeAttack");
                }
            }
            else
            {
                if(mPicker.PickWeapon(WeaponType.DoubleAxe, mAnimator))
                {
                    SetPickUpData(WeaponType.DoubleAxe);
                    UIManager.Instance().HideNotification();
                    mMovementScript.SetState(PlayerMovement.NavState.Paused);
                    mIKScript.SetIK(WeaponType.DoubleAxe);
                    AudioManager.PlaySFX("PickUp");
                    UIManager.Instance().ToggleWeapon(WeaponType.DoubleAxe);
                }
            }
        }
    }

    void HookUpdate()
    {
        if (mMovementScript.GetState() == PlayerMovement.NavState.Paused)
        {
            return;
        }
        if (!InputManager.IsHookPressed())
        {
            return;
        }
        if (mPicker.mAllowedWeapons[WeaponType.GrappleHook].mWeaponObject != null)
        {
            if (mPicker.mAllowedWeapons[WeaponType.GrappleHook].mPickedUp)
            {
                if (mPicker.AttackWeapon(WeaponType.GrappleHook, mAnimator))
                {
                    mMovementScript.SetState(PlayerMovement.NavState.Paused);
                    AudioManager.PlaySFX("HookProjectileAttack");
                }
            }
            else
            {
                if (mPicker.PickWeapon(WeaponType.GrappleHook, mAnimator))
                {
                    SetPickUpData(WeaponType.GrappleHook);
                    UIManager.Instance().HideNotification();
                    mMovementScript.SetState(PlayerMovement.NavState.Paused);
                    mIKScript.SetIK(WeaponType.GrappleHook);
                    AudioManager.PlaySFX("PickUp");
                    UIManager.Instance().ToggleWeapon(WeaponType.GrappleHook);
                }
            }
        }

    }

    void OneSwordUpdate()
    {
        if (mMovementScript.GetState() == PlayerMovement.NavState.Paused)
        {
            return;
        }
        if (!InputManager.IsMeeleePressed())
        {
            return;
        }
        if (mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null)
        {
            if (mPicker.mAllowedWeapons[WeaponType.OneSword].mPickedUp)
            {
                if (mPicker.AttackWeapon(WeaponType.OneSword, mAnimator))
                {
                    mMovementScript.SetState(PlayerMovement.NavState.Paused);
                    AudioManager.PlaySFX("SwordAttack");
                }
            }
            else
            {
                if (mPicker.PickWeapon(WeaponType.OneSword, mAnimator))
                {
                    SetPickUpData(WeaponType.OneSword);
                    UIManager.Instance().HideNotification();
                    mMovementScript.SetState(PlayerMovement.NavState.Paused);
                    mIKScript.SetIK(WeaponType.OneSword);
                    AudioManager.PlaySFX("PickUp");
                    UIManager.Instance().ToggleWeapon(WeaponType.OneSword);
                }
            }
        }
        else
        {
            if(mPicker.AttackWeapon(WeaponType.None, mAnimator))
            {
                mMovementScript.SetState(PlayerMovement.NavState.Paused);
                AudioManager.PlaySFX("MeeleeAttack");
                mMeelee.mActive = true;
            }
        }
    }

    void OnDestroy()
    {
        PersistentDataHandler.Instance().SetCurrentPlayer(null);
    }

    void SetPickUpData(WeaponType pType)
    {
        PlayerData aData = PersistentDataHandler.Instance().GetPlayerData();
        if (!aData.mWeaponsPicked.Contains(pType))
        {
            aData.mWeaponsPicked.Add(pType);
        }
        PersistentDataHandler.Instance().SetPlayerData(aData);
    }


    void Damage()
    {
        mMovementScript.SetState(PlayerMovement.NavState.Paused);
        mAnimator.SetInteger("ActionId", 1);
        mAnimator.SetInteger("EnemyId", (int)mDamageable.mDamagerType - 1);
        mAnimator.SetTrigger("ActionReaction");
    }

    void Die()
    {
        mMovementScript.SetState(PlayerMovement.NavState.Paused);
        mAnimator.SetInteger("ActionId", 0);
        mAnimator.SetInteger("EnemyId", (int)mDamageable.mDamagerType - 1);
        mAnimator.SetTrigger("ActionReaction");
        if(AudioManager.IsSoundPlaying("GameMusic"))
        {
            AudioManager.StopSound("GameMusic");
        }
        AudioManager.PlaySFX("GameOver");
        UIManager.Instance().ShowGameOver();
    }

    void ActionUpdates()
    {
        if (mAnimator.GetAnimatorTransitionInfo(0).IsName("FlyEnemyDamage -> Exit")
    || mAnimator.GetAnimatorTransitionInfo(0).IsName("WalkEnemyDamage -> Exit")
    || mAnimator.GetAnimatorTransitionInfo(0).IsName("JumpEnemyDamage -> Exit"))
        {
            mMovementScript.SetState(PlayerMovement.NavState.Idle);
        }
        DAction();
        BAction();
        DAAction();
        HPAction();
        OSAction();
    }

    void DAAction()
    {
        Weapon aWeapon = mPicker.mAllowedWeapons[WeaponType.DoubleAxe];
        if (mAnimator.GetAnimatorTransitionInfo(0).IsName("Entry -> GrabDoubleAxeStart"))
        {
            
        }
        else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabDoubleAxeStart -> GrabDoubleAxeEnd"))
        {
            aWeapon.mState = WeaponState.PickedUp;
            aWeapon.mKh = 0.25f;
            if (mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null
            && mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.activeInHierarchy)
            {
                mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.SetActive(false);
            }

        }
        else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabDoubleAxeEnd -> KeepAxeBehindAfterGrab"))
        {
            aWeapon.mKh = 1.0f;
        }
        else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("KeepAxeBehindAfterGrab -> Exit"))
        {
            aWeapon.mState = WeaponState.Holster;
            mMovementScript.SetState(PlayerMovement.NavState.Idle);
            if (mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null
            && !mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.activeInHierarchy)
            {
                mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.SetActive(true);
            }

        }
        else if (mAnimator.GetCurrentAnimatorStateInfo(0).IsName("DrawAxeBehind"))
        {
            aWeapon.mState = WeaponState.Attack;
            if(aWeapon.mAttackingTime <= 0.0f)
            {
                aWeapon.mAttackingTime = 3.55f;
                if(mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null
                && mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.activeInHierarchy)

                {
                    mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.SetActive(false);
                }
            }
        }
        else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("KeepAxeBehind -> Exit"))
        {
            aWeapon.mState = WeaponState.Holster;
            mMovementScript.SetState(PlayerMovement.NavState.Idle);
            aWeapon.mAttackingTime = 0.0f;
            if (mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null
            && !mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.activeInHierarchy)
            {
                mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.SetActive(true);
            }
        }
        if (aWeapon.mState == WeaponState.Attack)
        {
            aWeapon.mAttackingTime -= Time.deltaTime;
        }
        mPicker.mAllowedWeapons[WeaponType.DoubleAxe] = aWeapon;
    }

    void OSAction()
    {
        Weapon aWeapon = mPicker.mAllowedWeapons[WeaponType.OneSword];

        if (aWeapon.mWeaponObject == null)
        {
            if(mAnimator.GetAnimatorTransitionInfo(0).IsName("MeeleeAttack -> Exit"))
            {
                mMovementScript.SetState(PlayerMovement.NavState.Idle);
                mMeelee.mActive = false;
            }
        }
        else
        {
            if (mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabOneSwordStart -> GrabOneSwordEnd"))
            {
                aWeapon.mState = WeaponState.PickedUp;
                aWeapon.mKh = 1.0f;
            }
            else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabOneSwordEnd -> Exit"))
            {
                aWeapon.mState = WeaponState.Holster;
                mMovementScript.SetState(PlayerMovement.NavState.Idle);
            }
            else if (mAnimator.GetCurrentAnimatorStateInfo(0).IsName("SwordAttackStart"))
            {
                aWeapon.mState = WeaponState.Attack;
                if (aWeapon.mAttackingTime <= 0.0f)
                {
                    aWeapon.mAttackingTime = 2.5f;
                }
            }
            else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("SwordAttackEnd -> Exit"))
            {
                aWeapon.mState = WeaponState.Holster;
                mMovementScript.SetState(PlayerMovement.NavState.Idle);
                aWeapon.mAttackingTime = 0.0f;
            }
            if (aWeapon.mState == WeaponState.Attack)
            {
                aWeapon.mAttackingTime -= Time.deltaTime;
            }
            mPicker.mAllowedWeapons[WeaponType.OneSword] = aWeapon;
        }
    }

    void HPAction()
    {
        Weapon aWeapon = mPicker.mAllowedWeapons[WeaponType.GrappleHook];
        if(aWeapon.mState == WeaponState.AttackReturn)
        {
            mAnimator.SetBool("HookReturn", true);
        }
        if (mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabProjectileStart -> GrabProjectileEnd"))
        {
            aWeapon.mState = WeaponState.PickedUp;
            aWeapon.mKh = 1.0f;
            if (mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null
            && mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.activeInHierarchy)
            {
                mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.SetActive(false);
            }
        }
        else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("GrabProjectileEnd -> Exit"))
        {
            aWeapon.mWeaponObject.transform.Rotate(mPicker.transform.right, 90);
            aWeapon.mState = WeaponState.Holster;
            mMovementScript.SetState(PlayerMovement.NavState.Idle);
            if (mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null
            && !mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.activeInHierarchy)
            {
                mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.SetActive(true);
            }

        }
        else if (mAnimator.GetCurrentAnimatorStateInfo(0).IsName("ProjectileThrow"))
        {
            aWeapon.mState = WeaponState.Attack;
            if (mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null
                && mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.activeInHierarchy)
            {
                mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.SetActive(false);
            }
        }
        else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("ProjectileThrow -> ProjectileStay"))
        {
            if (aWeapon.mAttackingTime <= 0.0f)
            {
                aWeapon.mAttackingTime = 0.45f;
            }
        }
        else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("ProjectileStay -> ProjectileReturn"))
        {

        }
        else if (mAnimator.GetAnimatorTransitionInfo(0).IsName("ProjectileReturn -> Exit"))
        {
            aWeapon.mAttackingTime = 0.0f;
            aWeapon.mState = WeaponState.Holster;
            mMovementScript.SetState(PlayerMovement.NavState.Idle);
            mAnimator.SetBool("HookReturn", false);
            if (mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null
            && !mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.activeInHierarchy)
            {
                mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject.gameObject.SetActive(true);
            }
        }
        if (aWeapon.mState == WeaponState.Attack || aWeapon.mState == WeaponState.AttackReturn)
        {
            aWeapon.mAttackingTime -= Time.deltaTime;
            if(aWeapon.mState == WeaponState.Attack && aWeapon.mAttackingTime <= 0.0f)
            {
                aWeapon.mAttackingTime = 0.5f;
                aWeapon.mState = WeaponState.AttackReturn;
            }
        }
        mPicker.mAllowedWeapons[WeaponType.GrappleHook] = aWeapon;
    }

    void BAction()
    {
        if(mBroom == null)
        {
            return;
        }
        if (!InputManager.IsBroomPressed())
        {
            if(mBroom.mIsEquipped && mBroom.mCanMove)
            {
                UnEquipBroom();
            }
        }
        if(mBroom.mCurTime <= 0.0f)
        {
            mBroom.mCurTime = 0.0f;
            UnEquipBroom();
        }
        if(mAnimator.GetAnimatorTransitionInfo(0).IsName("OnBroom -> StayBroom"))
        {
            transform.position = new Vector3(mBroom.transform.position.x, mBroom.transform.position.y - 0.6f, mBroom.transform.position.z);
            transform.SetParent(mBroom.transform);
            mBroom.AllowBroomToMove();
        }
        else if(mAnimator.GetAnimatorTransitionInfo(0).IsName("StayBroom -> Exit"))
        {
            mBroom.mIsEquipped = false;
            mMainBodyCollider.center = mMovementScript.mColliderCenter;
            mMainBodyCollider.height = mMovementScript.mColliderHeight;
            foreach (CapsuleCollider aShoenHand in mShoesAndHands)
            {
                aShoenHand.enabled = true;
            }
            mRigidBody.isKinematic = false;
            mAnimator.SetBool("OffBroom", false);
            mMovementScript.SetState(PlayerMovement.NavState.Idle);
        }
    }



    void DoorUpdate()
    {
        if (mMovementScript.GetState() == PlayerMovement.NavState.Paused)
        {
            return;
        }
        if(!InputManager.IsDoubleAxePressed())
        {
            return;
        }
        if(mActiveGate == null)
        {
            return;
        }
        mActiveGate.OpenGate(mAnimator,PersistentDataHandler.Instance().GetPlayerData().mKeyCollected);
        mMovementScript.SetState(PlayerMovement.NavState.Paused);
    }

    void DAction()
    {
        if(mActiveGate == null)
        {
            return;
        }
        if(mAnimator.GetCurrentAnimatorStateInfo(0).IsName("Open Door"))
        {
            if (mCollectedKey != null)
            {
                mCollectedKey.gameObject.SetActive(true);
            }
        }
        if (mAnimator.GetAnimatorTransitionInfo(0).IsName("Open Door -> Exit"))
        {
            if (mCollectedKey != null)
            {
                mCollectedKey.gameObject.SetActive(false);
            }
            mMovementScript.SetState(PlayerMovement.NavState.Idle);
            mActiveGate = null;
        }
        if(mAnimator.GetAnimatorTransitionInfo(0).IsName("CantOpenEnd -> Exit"))
        {
            mMovementScript.SetState(PlayerMovement.NavState.Idle);
            mActiveGate = null;
        }
    }

}
