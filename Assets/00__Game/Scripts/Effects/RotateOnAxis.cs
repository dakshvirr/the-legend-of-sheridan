﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnAxis : MonoBehaviour
{
    [SerializeField]
    bool xAxis = false;
    [SerializeField]
    bool yAxis = false;
    [SerializeField]
    bool zAxis = false;

    float rotationAngleX = 3;
    float rotationAngleY = 3;
    float rotationAngleZ = 3;

    Vector3 rotateAngle;
    void Start()
    {
        if (!xAxis)
            rotationAngleX = 0;

        if (!yAxis)
            rotationAngleY = 0;

        if (!zAxis)
            rotationAngleZ = 0;

        rotateAngle = new Vector3(rotationAngleX, rotationAngleY, rotationAngleZ);

        InvokeRepeating("StartRotation",0.1f,0.1f);
    }

    void StartRotation()
    {
        gameObject.transform.Rotate(rotateAngle);
    }
}
