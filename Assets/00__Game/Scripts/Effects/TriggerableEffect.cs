﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerableEffect : Mixin
{
    ParticleSystem mSystem;
    void Start()
    {
        mSystem = GetComponent<ParticleSystem>();
    }
    public override void Dispatch(GameObject pObject)
    {
        if(pObject.GetComponent<Player>() != null || pObject.GetComponent<Enemy>() != null)
        {
            mSystem.Stop();
        }
    }

    public override void ResetDispatch(GameObject pObject)
    {
        if(!mSystem.isPlaying)
        {
            mSystem.Play();
        }
    }

}
