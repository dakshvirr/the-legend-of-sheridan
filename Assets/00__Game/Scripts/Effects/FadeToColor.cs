﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Fade To Color class is used to fade in and fade out canvas groups completely
/// </summary>
public class FadeToColor : MonoBehaviour
{
    [SerializeField] private CanvasGroup mFader;
    /// <summary>
    /// Called to fade in and out the gameObject
    /// </summary>
    /// <param name="pFadeIn">fade in if set to true</param>
    /// <param name="pTime">time taken to fade in/out</param>
    /// <param name="pDelay">delay before fade in/out</param>
    public void Fade(bool pFadeIn, float pTime, float pDelay)
    {
        if(pFadeIn)
        {
            mFader.blocksRaycasts = mFader.interactable = true;
            LeanTween.alphaCanvas(mFader, 1f, pTime).setDelay(pDelay).setIgnoreTimeScale(true);
        }
        else
        {
            LeanTween.alphaCanvas(mFader, 0f, pTime).setDelay(pDelay).setOnComplete(() => 
            {
                mFader.blocksRaycasts = mFader.interactable = false;
            }).setIgnoreTimeScale(true);
        }
    }
}
