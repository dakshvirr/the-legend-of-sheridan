﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public GameObject mFireball;
    private GameObject mFb;
    public Transform mMouth;
    Vector3 mPos;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        FireballUpdate();   
    }

    void FireballUpdate()
    {
        if(mFb!=null)
        {
            mFb.transform.position = Vector3.MoveTowards(mFb.transform.position, mPos, 5.0f * Time.deltaTime);
            if(Vector3.Distance(mFb.transform.position, mPos) < 0.5f)
            {
                Destroy(mFb);
            }
        }
    }

    void CreateFireball()
    {
        mFb = Instantiate(mFireball,mMouth.position,mMouth.rotation);
        mPos = PersistentDataHandler.Instance().GetPlayerPosition();
    }

 
}
