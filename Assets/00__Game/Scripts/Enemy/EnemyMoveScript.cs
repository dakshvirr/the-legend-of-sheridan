﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMoveScript : MonoBehaviour
{
    Enemy mEnemy;
    public List<Transform> mWayPoints;
    int mcurrentWayPoint = 0;
    public float mFollowDistance = 4.0f;

    void Start()
    {
        mEnemy = GetComponent<Enemy>();

    }

    void Update()
    {
        EnemyMoveUpdate();

    }

    void EnemyMoveUpdate()
    {
        if (mEnemy.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("Move"))
        {
            if (!mEnemy.mEnemyNav.isStopped)
            {
                if (Vector3.Distance(this.transform.position, PersistentDataHandler.Instance().GetPlayerPosition()) >= mFollowDistance)
                {
                    if (mWayPoints.Count != 0)
                    {
                        if (Vector3.Distance(mWayPoints[mcurrentWayPoint].transform.position, this.transform.position) < mEnemy.mEnemyNav.stoppingDistance)
                        {
                            mcurrentWayPoint++;
                            if (mcurrentWayPoint >= mWayPoints.Count)
                            {
                                mcurrentWayPoint = 0;
                            }
                        }
                        mEnemy.mEnemyNav.SetDestination(mWayPoints[mcurrentWayPoint].transform.position);
                    }
                }
                else
                {
                    mEnemy.mEnemyNav.SetDestination(PersistentDataHandler.Instance().GetPlayerPosition());
                }
            }
        }
    }
}
