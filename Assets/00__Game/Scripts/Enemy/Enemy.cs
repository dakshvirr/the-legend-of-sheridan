﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [Header("Unity Components")]
    public Animator mAnimator;
    public Rigidbody mRigidBody;
    public CapsuleCollider mMainBodyCollider;
    public NavMeshAgent mEnemyNav;

    [Header("Custom Scripts")]
    public WeaponPicker mPicker;
    public Fireball mFireball;
    public Damageable mDamageable;

    public float mHealth;
    public float mDestroyTime = 2.0f;
    [HideInInspector]
    public bool mDead = false;
    [HideInInspector]
    public bool mAttacking = false;
    [HideInInspector]
    public bool mInteracting = false;
    void Start()
    {
        if(mPicker != null)
        {
            mPicker.mAllowedWeapons = new Dictionary<WeaponType, Weapon>();
            mPicker.mAllowedWeapons.Add(WeaponType.OneSword, new Weapon(false, null));
        }
    }

    void Update()
    {
        if(!mAttacking && !mDead && !mInteracting)
        {
            mEnemyNav.isStopped = false;
        }
        else
        {
            mEnemyNav.isStopped = true;
        }
        DieUpdate();
        if(mAnimator.GetAnimatorTransitionInfo(0).IsName("Hurt -> Move"))
        {
            mAnimator.SetBool("Hurt", false);

        }
        if (mAnimator.GetAnimatorTransitionInfo(0).IsName("PickUp -> Move"))
        {
            mAnimator.SetBool("PickUpEn", false);
        }

        SwordUpdate();    
    }

    private void SwordUpdate()
    {
        if(mPicker!=null)
        {
            if (mPicker.mAllowedWeapons[WeaponType.OneSword].mWeaponObject != null)
            {
                if (!mPicker.mAllowedWeapons[WeaponType.OneSword].mPickedUp)
                {
                    if (mPicker.PickWeapon(WeaponType.OneSword, mAnimator))
                    {
                        Weapon aWeapon = mPicker.mAllowedWeapons[WeaponType.OneSword];
                        aWeapon.mKh = 1.0f;
                        aWeapon.mState = WeaponState.PickedUp;
                        mPicker.mAllowedWeapons[WeaponType.OneSword] = aWeapon;
                        mAnimator.SetBool("PickUpEn", true);
                    }
                }
                if (mAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pick Up"))
                {
                    mInteracting = true;
                }
                else
                {
                    mInteracting = false;
                }
            }
        }
    }

    void DieUpdate()
    {
        if (mDamageable.mCurrentDamage > 0 && (mDamageable.mDamagerType == Damageable.DamagerType.WalkEnemy))
        {
            if (mHealth > 0)
            {
                mHealth -= mDamageable.mCurrentDamage;
                if (mHealth <= 0)
                {
                    if (!mDead)
                    {
                        PlayerData aData = PersistentDataHandler.Instance().GetPlayerData();
                        aData.mScore += 2 * (int)mDamageable.mCurrentDamage;
                        UIManager.Instance().UpdateScore(aData.mScore);
                        PersistentDataHandler.Instance().SetPlayerData(aData);
                        Die();
                    }
                }
                else
                {
                    PlayerData aData = PersistentDataHandler.Instance().GetPlayerData();
                    aData.mScore += (int)mDamageable.mCurrentDamage;
                    UIManager.Instance().UpdateScore(aData.mScore);
                    PersistentDataHandler.Instance().SetPlayerData(aData);
                    Damage();
                }
            }
            mDamageable.mCurrentDamage = -1;
            mDamageable.mDamagerType = Damageable.DamagerType.None;
        }
    }

    void Die()
    {
        mDead = true;
        mAnimator.SetTrigger("isDead");
        Destroy(this.gameObject, mDestroyTime);
        mDead = true;
    }

    void Damage()
    {
        mAnimator.SetBool("Hurt",true);
    }

}

