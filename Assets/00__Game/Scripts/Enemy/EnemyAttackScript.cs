﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackScript : MonoBehaviour
{
    Enemy mEnemy;
    float mAttackingDistance;
   public float mSoldierAttackingDistance = 1.0f;
   public float mGruntAttackingDistance = 3.0f;
   public float mDragonAttackingDistance = 4.0f;
    bool isAttacking = false;
    public EnemyAttackTrigger mTrigger;
    // Start is called before the first frame update
    void Start()
    {
        mEnemy = GetComponent<Enemy>();
        if (mEnemy.mPicker != null)
        {
            mAttackingDistance = mSoldierAttackingDistance;
        }
        else if (mEnemy.mFireball != null)
        {
            mAttackingDistance = mDragonAttackingDistance;
        }
        else
        {
            mAttackingDistance = mGruntAttackingDistance;
        }
    }

    // Update is called once per frame
    void Update()
    {
        EnemyAttackUpdate();
    }

    void EnemyAttackUpdate()
    {

        if(mEnemy.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("Move"))
        {
            if (Vector3.Distance(this.transform.position, PersistentDataHandler.Instance().GetPlayerPosition()) < mAttackingDistance)
            {
                if(!isAttacking)
                {
                    mEnemy.mAnimator.SetBool("Attack", true);
                    mEnemy.mAttacking = true;
                    isAttacking = true;
                    if(mTrigger != null)
                    {
                        if (mEnemy.mPicker != null && mEnemy.mPicker.mAllowedWeapons[WeaponType.OneSword].mPickedUp)
                        {
                            mTrigger.mActive = false;
                            Weapon aWeapon = mEnemy.mPicker.mAllowedWeapons[WeaponType.OneSword];
                            aWeapon.mState = WeaponState.Attack;
                            mEnemy.mPicker.mAllowedWeapons[WeaponType.OneSword] = aWeapon;
                        }
                        else
                        {
                            mTrigger.mActive = true;
                        }

                    }
                }
            }
        }

      else if (mEnemy.mAnimator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            mEnemy.transform.LookAt(PersistentDataHandler.Instance().GetPlayerPosition());

            if (mEnemy.mPicker != null)
            {
               
                //soldier logic
            }
            else if(mEnemy.mFireball !=null)
            {
               //Dragon logic
            }
            else
            {
                if(mEnemy.mRigidBody != null)
                {
                    transform.position = Vector3.MoveTowards(transform.position, PersistentDataHandler.Instance().GetPlayerPosition(), 2.0f * Time.deltaTime);
                }
            }

            if (Vector3.Distance(this.transform.position, PersistentDataHandler.Instance().GetPlayerPosition()) > mAttackingDistance)
            {
                if (isAttacking)
                {
                    mEnemy.mAnimator.SetBool("Attack", false);
                    mEnemy.mAttacking = false;
                    isAttacking = false;
                    if(mTrigger != null)
                    {
                        mTrigger.mActive = false;
                        if (mEnemy.mPicker != null && mEnemy.mPicker.mAllowedWeapons[WeaponType.OneSword].mPickedUp)
                        {
                            Weapon aWeapon = mEnemy.mPicker.mAllowedWeapons[WeaponType.OneSword];
                            aWeapon.mState = WeaponState.Holster;
                            mEnemy.mPicker.mAllowedWeapons[WeaponType.OneSword] = aWeapon;
                        }
                    }
                }
            }
        }
    }
}
