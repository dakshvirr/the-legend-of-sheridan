﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackTrigger : MonoBehaviour
{ 

    public bool mActive;
    public float mDamage;
    public Damageable.DamagerType mType;  

    void OnTriggerEnter(Collider other)
    {
        if(!mActive)
        {
            return;
        }
        if(other.attachedRigidbody == null)
        {
            return;
        }
        Player aPlayer = other.attachedRigidbody.gameObject.GetComponent<Player>();
        if(aPlayer == null)
        {
            return;
        }
        if(aPlayer.mDamageable.mCurrentDamage <= 0)
        {
            aPlayer.mDamageable.Damage(mDamage, mType);
        }
    }

}
