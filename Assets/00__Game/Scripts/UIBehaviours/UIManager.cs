﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    static UIManager mInstance;

    [SerializeField]
    GameObject mNotificationPanel;
    [SerializeField]
    Text mNotificationMessageText;

    [SerializeField]
    Slider mHealthSlider;
    [SerializeField]
    Text mScoreText;
    [SerializeField]
    Slider mBroomSlider;
    [SerializeField]
    Toggle mSwordToggle;
    [SerializeField]
    Toggle mAxeToggle;
    [SerializeField]
    Toggle mGrappleToggle;

    [SerializeField]
    GameObject mHUDObject;
    [SerializeField]
    GameObject mPauseMenuObject;
    [SerializeField]
    GameObject mGameOverMenuObject;

    void Awake()
    {
        if (mInstance == null)
        {
            mInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        InvokeRepeating("CheckUpdate", 0.1f, 0.1f);
    }

    public static UIManager Instance()
    {
        return mInstance;
    }

    #region NOTIFICATION

    public void ShowNotification(string pMessage)
    {
        mNotificationMessageText.text = pMessage;
        mNotificationPanel.SetActive(true);
    }

    public void HideNotification()
    {
        mNotificationPanel.SetActive(false);
        mNotificationMessageText.text = "";
    }
    #endregion

    #region HUD

    public void UpdateHealth(float pHealth)
    {
        mHealthSlider.value = pHealth;
    }
    public void UpdateBroom(float pHover)
    {
        mBroomSlider.value = pHover;
    }

    public void ToggleWeapon(WeaponType pWeapon)
    {
        switch(pWeapon)
        {
            case WeaponType.DoubleAxe:
                mAxeToggle.isOn = !mAxeToggle.isOn;
                break;
            case WeaponType.OneSword:
                mSwordToggle.isOn = !mSwordToggle.isOn;
                break;
            case WeaponType.GrappleHook:
                mGrappleToggle.isOn = !mGrappleToggle.isOn;
                break;
        }
    }

    public void UpdateScore(int pScore)
    {
        mScoreText.text = "SCORE : " + pScore;
    }

    #endregion

    #region MENUS

    public void ShowPause()
    {
        //Do State Change here
        LevelManager.Instance().mCurrentGameState = GameStates.PAUSE;

        mHUDObject.SetActive(false);
        mPauseMenuObject.SetActive(true);
        //mGameOverMenuObject.SetActive()
    }

    public void HidePauseMenu()
    {
        //Do State Change here
        LevelManager.Instance().mCurrentGameState = GameStates.PLAY;
        
        mHUDObject.SetActive(true);
        mPauseMenuObject.SetActive(false);
    }

    public void ShowHUD()
    {
        mHUDObject.SetActive(true);
    }
    public void HideHUD()
    {
        mHUDObject.SetActive(false);
    }

    public void ShowGameOver()
    {
        //Do State change here
        LevelManager.Instance().mCurrentGameState = GameStates.GAMEOVER;

        mPauseMenuObject.SetActive(false);
        mGameOverMenuObject.SetActive(true);
        HideHUD();
    }

    public void HideGameOver()
    {
        LevelManager.Instance().mCurrentGameState = GameStates.START;

        mPauseMenuObject.SetActive(false);
        mGameOverMenuObject.SetActive(false);
        HideHUD();
    }

    private void CheckUpdate()
    {
        if (InputManager.IsBackButtonPressed())
        {
            if(LevelManager.Instance().mCurrentGameState == GameStates.PLAY)
            ShowPause();
            //else if(LevelManager.Instance().mCurrentGameState == GameStates.PAUSE)
            //HidePauseMenu();
        }
    }

    #endregion

    private void OnLevelWasLoaded(int level)
    {
        if (level > 0)
        {
            ShowHUD();
        }
        else
        {
            HideHUD();
        }
    }
}
