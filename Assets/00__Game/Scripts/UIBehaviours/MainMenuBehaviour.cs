﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuBehaviour : MonoBehaviour
{
    [SerializeField]
    Button[] mMenuButtons;
    int mCurrentSelectedIndex = 0;

    private void Start()
    {
        mMenuButtons[0].Select();
    }

    public void LoadNewGame()
    {
        foreach (Button b in mMenuButtons)
        {
            b.interactable = false;
        }
        LevelManager.Instance().LoadLevelAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name,"OverWorld");
    }

    public void QuitGame()
    {
#if !UNITY_EDITOR
        Application.Quit();
#else
        Debug.Log("Quitting");
#endif
    }

    //private IEnumerator CheckInput()
    //{
    //    yield return new WaitForSeconds(0.1f);
    //    UpdateSelection();
    //}

    //void SelectAction()
    //{
    //    mMenuButtons[mCurrentSelectedIndex].onClick.Invoke();
    //}

    //public void UpdateSelection()
    //{
    //    if (InputManager.GetLeftY() < 0)
    //    {
    //        mCurrentSelectedIndex++;
    //        if (mCurrentSelectedIndex > mMenuButtons.Length)
    //        {
    //            mCurrentSelectedIndex = 0;
    //        }
    //    }
    //    else if (InputManager.GetLeftY() > 0)
    //    {
    //        mCurrentSelectedIndex--;
    //        if (mCurrentSelectedIndex < 0)
    //        {
    //            mCurrentSelectedIndex = mMenuButtons.Length - 1;
    //        }
    //    }
    //    Debug.Log(mCurrentSelectedIndex);
    //    mMenuButtons[mCurrentSelectedIndex].Select();
    //    //return mCurrentSelectedIndex;
    //}
}
