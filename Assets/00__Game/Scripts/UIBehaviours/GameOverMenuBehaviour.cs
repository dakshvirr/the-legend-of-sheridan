﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverMenuBehaviour : MonoBehaviour
{
    [SerializeField]
    UnityEngine.UI.Button mRestartButton;
    [SerializeField]
    UnityEngine.UI.Button mQuitToMainButton;

    private void OnEnable()
    {
        mRestartButton.Select();
    }

    public void RestartGame()
    {
        UIManager.Instance().HideGameOver();
        PersistentDataHandler.Instance().SetPlayerData(new PlayerData(0, 100));
        LevelManager.Instance().LoadLevelAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name, "OverWorld");
    }

    // Update is called once per frame
    public void QuitToMain()
    {
        UIManager.Instance().HideGameOver();
        PersistentDataHandler.Instance().SetPlayerData(new PlayerData(0, 100));
        LevelManager.Instance().LoadLevelAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name, "StartGame");
    }
}
