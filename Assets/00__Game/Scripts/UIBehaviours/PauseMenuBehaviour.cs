﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuBehaviour : MonoBehaviour
{
    [SerializeField]
    UnityEngine.UI.Button mResumeButton;
    [SerializeField]
    UnityEngine.UI.Button mQuitToMainButton;

    private void OnEnable()
    {
        mResumeButton.Select();
    }


    public void ResumeGame()
    {
        UIManager.Instance().HidePauseMenu();
    }

    public void QuitToMain()
    {
        UIManager.Instance().HidePauseMenu();
        LevelManager.Instance().mCurrentGameState = GameStates.START;
        PersistentDataHandler.Instance().SetPlayerData(new PlayerData(0, 100));
        LevelManager.Instance().LoadLevelAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name, "StartGame");
    }

}
