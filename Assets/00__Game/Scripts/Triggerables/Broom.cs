﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Broom : Mixin
{
    public float mYMountPos;
    public float mYUnmountPos;

    public float mSpeed;
    public float mProjectileCooldownTime;

    public Transform mLeftLegHandle;
    public Transform mRightLegHandle;
    public Transform mLeftHandHandle;
    public Transform mRightHandHandle;

    public Triggerable mTriggerable;
    public GameObject mRenderable;
    public Rigidbody mRigidBody;
    public BoxCollider mCollider;


    public float mMaxTime;
    public float mTimeIncSpeed;

    [HideInInspector]
    public float mCurTime = 0.0f;
    [HideInInspector]
    public bool mPickedUp = false;
    [HideInInspector]
    public bool mIsEquipped = false;
    [HideInInspector]
    public bool mCanMove = false;
    Player mPlayer = null;
    float mProjTime;

    public GameObject mProjectile;

    void Start()
    {
        mCurTime = mMaxTime;
    }

    void Update()
    {
        if (LevelManager.Instance().mCurrentGameState != GameStates.PLAY)
        {
            return;
        }
        if (!mPickedUp)
        {
            return;
        }
        UIManager.Instance().UpdateBroom(mCurTime);
        if(mPickedUp && !mIsEquipped)
        {
            if(mRenderable.activeInHierarchy)
            {
                mRenderable.SetActive(false);
            }
        }
        if (!mIsEquipped)
        {
            mCurTime += Time.deltaTime * mTimeIncSpeed;
            if(mCurTime > mMaxTime)
            {
                mCurTime = mMaxTime;
            }
        }
        else if(mIsEquipped && mCanMove)
        {
            mCurTime -= Time.deltaTime;
            HandleMovement();
        }
    }

    void HandleMovement()
    {
        float aForward = InputManager.GetLeftY();
        float aTurn = InputManager.GetLeftX();
        if(Mathf.Abs(aForward) > 0.1 || Mathf.Abs(aTurn) > 0.1)
        {
            if(!AudioManager.IsSoundPlaying("BroomFly"))
            {
                AudioManager.PlaySFX("BroomFly");
            }
        }
        if(Mathf.Abs(aForward) > 0.1f)
        {
            transform.Translate(aForward * Time.deltaTime * mSpeed * (Quaternion.Euler(0, -90, 0) * (transform.rotation * Vector3.forward)));
        }
        if(Mathf.Abs(aTurn) > 0.1f)
        {
            transform.Rotate(transform.up, aTurn * Time.deltaTime * mSpeed);
        }
        mProjTime -= Time.deltaTime;
        if(InputManager.IsMeeleePressed() && mProjTime <= 0.0f)
        {
            mProjTime = 1.0f;
            GameObject aProj = Instantiate(mProjectile, transform, false);
            aProj.transform.SetParent(null);
            BroomProjectile aProjectile = aProj.GetComponent<BroomProjectile>();
            aProjectile.InstantiateProjectile();
        }
    }


    public override void Dispatch(GameObject pObject)
    {
        if (mPlayer != null)
        {
            return;
        }
        mPlayer = pObject.GetComponent<Player>();
        if (mPlayer == null)
        {
            return;
        }
        if(mPlayer.mBroom != null)
        {
            mPlayer = null;
            return;
        }
        else if (mPlayer.mMovementScript.GetState() == PlayerMovement.NavState.Paused)
        {
            return;
        }
        UIManager.Instance().ShowNotification("Keep LT Pressed to ride broom");
        mPlayer.mBroom = this;
        mTriggerable.DisableTriggerable();
    }

    public override void ResetDispatch(GameObject pObject)
    {
        if (mPlayer.gameObject != pObject)
        {
            return;
        }
        UIManager.Instance().HideNotification();
        mPlayer.mBroom = null;
        mPlayer = null;
        mIsEquipped = false;
    }

    public void RemoveTriggerable()
    {
        mTriggerable.DontActivateOnExit();
        mTriggerable.gameObject.SetActive(false);
    }

    public bool IsTriggerable()
    {
        return mTriggerable.gameObject.activeInHierarchy;
    }

    public void AllowBroomToMove()
    {
        StartCoroutine(GetToFloatingPoint());
    }

    IEnumerator GetToFloatingPoint()
    {
        while(transform.localPosition.y < mYMountPos)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,new Vector3(transform.localPosition.x, mYMountPos + 0.2f, transform.localPosition.z),Time.deltaTime);
            yield return null;
        }
        mProjTime = 0.0f;
        mCollider.enabled = true;
        mRigidBody.isKinematic = false;
        mCanMove = true;
    }

}
