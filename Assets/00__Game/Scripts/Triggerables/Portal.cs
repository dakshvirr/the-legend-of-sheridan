﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [SerializeField]
    string mLevelToLoad = "OverWorld";
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Player>() != null)
        {
            AudioManager.PlaySFX("PortalSound");
            LevelManager.Instance().LoadLevelAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name, mLevelToLoad);
        }
    }
}
