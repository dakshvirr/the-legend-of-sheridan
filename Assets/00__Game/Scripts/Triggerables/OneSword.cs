﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneSword : Weapons
{
    public Transform mRightHandHandle;

    public override void FireWeapon()
    {
    }

    void Start()
    {
        mType = WeaponType.OneSword;
    }

    void Update()
    {
        if (mActivePicker != null && mTriggerable.gameObject.activeInHierarchy)
        {
            if (mActivePicker.mAllowedWeapons[mType].mPickedUp)
            {
                mTriggerable.gameObject.SetActive(false);
            }
        }
        if (mActivePicker != null)
        {
            if (mActivePicker.mAllowedWeapons[mType].mState != WeaponState.None)
            {
                AnchorToHand();
            }
        }
    }

    void AnchorToHand()
    {
        transform.position += mActivePicker.mAllowedWeapons[mType].mKh * (mActivePicker.mWeaponAnchor.position - transform.position);
        if (mActivePicker.mAllowedWeapons[mType].mState != WeaponState.PickedUp)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, mActivePicker.mWeaponAnchor.rotation, mActivePicker.mAllowedWeapons[mType].mKh);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (mActivePicker == null)
        {
            return;
        }
        if (!(mActivePicker.mAllowedWeapons[mType].mState == WeaponState.Attack))
        {
            return;
        }
        if (other.attachedRigidbody == null)
        {
            return;
        }
        if(other.attachedRigidbody.gameObject == mActivePicker.transform.parent.gameObject)
        {
            return;
        }
        Damageable aDamageable = other.attachedRigidbody.gameObject.GetComponent<Damageable>();
        if(aDamageable == null)
        {
            return;
        }

        aDamageable.Damage(mWeaponDamage, Damageable.DamagerType.WalkEnemy);


    }
}
