﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookProjectile : Weapons
{
    public LineRenderer mHookChain;
    public Transform mLinePoint;
    public Transform mRightHandHandle;
    public GameObject mRenderable;

    public override void FireWeapon()
    {

    }

    void Start()
    {
        mType = WeaponType.GrappleHook;
    }

    void Update()
    {
        if (mActivePicker != null && mTriggerable.gameObject.activeInHierarchy)
        {
            if (mActivePicker.mAllowedWeapons[mType].mPickedUp)
            {
                mTriggerable.gameObject.SetActive(false);
            }
        }
        if (mActivePicker != null)
        {
            if (mActivePicker.mAllowedWeapons[mType].mState == WeaponState.PickedUp)
            {
                AnchorToHand();
            }
            else if(mActivePicker.mAllowedWeapons[mType].mState == WeaponState.Attack
                || mActivePicker.mAllowedWeapons[mType].mState == WeaponState.AttackReturn)
            {
                if(mActivePicker.mAllowedWeapons[mType].mAttackingTime <= 0.0f)
                {
                    AnchorToHand();
                }
                else
                {
                    LineDraw(mActivePicker.mAllowedWeapons[mType].mState == WeaponState.AttackReturn ? -1.85f : 1);
                }
            }
            if (mActivePicker.mAllowedWeapons[mType].mState == WeaponState.Holster && mRenderable.activeInHierarchy)
            {
                mRenderable.SetActive(false);
                AnchorToHand();
                mHookChain.positionCount = 0;
            }
            else if(mActivePicker.mAllowedWeapons[mType].mState != WeaponState.Holster && !mRenderable.activeInHierarchy)
            {
                mRenderable.SetActive(true);
            }
        }
    }

    void AnchorToHand()
    {
        transform.position += mActivePicker.mAllowedWeapons[mType].mKh * (mActivePicker.mWeaponAnchor.position - transform.position);
    }

    void LineDraw(float pSign)
    {
        transform.position += (mActivePicker.transform.forward * Time.deltaTime * 10.0f * pSign);
        mHookChain.positionCount = 2;
        mHookChain.SetPositions(new Vector3[] { mActivePicker.mWeaponAnchor.position, mLinePoint.position });
    }

    void OnTriggerEnter(Collider other)
    {
        if (mActivePicker == null)
        {
            return;
        }
        if (!(mActivePicker.mAllowedWeapons[mType].mState == WeaponState.Attack ||
            mActivePicker.mAllowedWeapons[mType].mState == WeaponState.AttackReturn))
        {
            return;
        }
        if (other.attachedRigidbody == null)
        {
            return;
        }

        if (other.attachedRigidbody.gameObject == mActivePicker.transform.parent.gameObject)
        {
            return;
        }

        Damageable aDamageable = other.attachedRigidbody.gameObject.GetComponent<Damageable>();
        if (aDamageable == null)
        {
            return;
        }

        aDamageable.Damage(mWeaponDamage, Damageable.DamagerType.WalkEnemy);

        if(mActivePicker.mAllowedWeapons[mType].mState == WeaponState.Attack)
        {
            Weapon aWeapon = mActivePicker.mAllowedWeapons[mType];
            aWeapon.mAttackingTime = 0;
            mActivePicker.mAllowedWeapons[mType] = aWeapon;
        }

    }

}
