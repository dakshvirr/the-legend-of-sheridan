﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : Mixin
{
    public Triggerable mTriggerable;

    public override void Dispatch(GameObject pObject)
    {
        Player aPlayer = pObject.GetComponent<Player>();
        if (aPlayer == null)
        {
            return;
        }
        else
        {
            PlayerData aData = PersistentDataHandler.Instance().GetPlayerData();
            if(aData.mKeyCollected)
            {
                return;
            }
            AudioManager.PlaySFX("PickUp");
            aData.mKeyCollected = true;
            PersistentDataHandler.Instance().SetPlayerData(aData);
            aPlayer.mCollectedKey = this;
            transform.SetParent(aPlayer.mPicker.mWeaponAnchor.transform);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            RemoveTriggerable();
            gameObject.SetActive(false);
        }
    }

    public override void ResetDispatch(GameObject pObject)
    {

    }

    public void RemoveTriggerable()
    {
        mTriggerable.DontActivateOnExit();
        mTriggerable.gameObject.SetActive(false);
    }

}
