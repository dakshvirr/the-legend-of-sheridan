﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TriggerEvents : UnityEvent<GameObject> { }

[RequireComponent(typeof(Collider))]
public class Triggerable : MonoBehaviour
{
    public List<TriggerEvents> mEvents;
    public List<TriggerEvents> mResetEvents;
    public Collider mCollider;
    bool mIsEnabled = true;
    bool mActivateOnTriggerExit = false;
    
	void OnTriggerEnter(Collider other)
    {
        if(other.attachedRigidbody == null)
        {
            return;
        }
        if (!mIsEnabled || other.attachedRigidbody.gameObject != other.gameObject)
        {
            return;
        }
        foreach (TriggerEvents aEvent in mEvents)
        {
            if(aEvent != null)
            {
                aEvent.Invoke(other.attachedRigidbody.gameObject);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody == null)
        {
            return;
        }
        if(other.attachedRigidbody.gameObject != other.gameObject)
        {
            return;
        }
        if (mActivateOnTriggerExit)
        {
            mIsEnabled = true;
            mActivateOnTriggerExit = false;
            foreach(TriggerEvents aEvent in mResetEvents)
            {
                if(aEvent != null)
                {
                    aEvent.Invoke(other.gameObject);
                }
            }
        }

    }

    public bool IsEnabled()
    {
        return mIsEnabled;
    }

    public void DisableTriggerable(bool pEnable = false)
    {
        mIsEnabled = pEnable;
        mActivateOnTriggerExit = true;
    }

    public void DontActivateOnExit()
    {
        mActivateOnTriggerExit = false;
    }
}
