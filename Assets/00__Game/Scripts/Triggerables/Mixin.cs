﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Mixin : MonoBehaviour
{
    public abstract void Dispatch(GameObject pObject);
    public abstract void ResetDispatch(GameObject pObject);
}
