﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FenceGate : Mixin
{
    public enum State
    {
        None,
        Closed,
        Opening,
        Opened
    }
    public bool mUnlocked;
    public Triggerable mTriggerable;
    Player mPlayer;
    State mState = State.Closed;

    public Transform mLeftHandle;
    public Transform mRightHandle;

    public Transform mPivotLeft;
    public Transform mPivotRight;

    bool mAnimating = false;

    void Update()
    {
        if(mPlayer != null && !mAnimating)
        {
            if(mState == State.Opening)
            {
                mAnimating = true;
                LeanTween.rotateLocal(mPivotRight.gameObject, new Vector3(0,90.0f,0), 1.5f);
                LeanTween.rotateLocal(mPivotLeft.gameObject, new Vector3(0,-90.0f,0), 1.5f).setOnComplete(() => {
                    mState = State.Opened;
                    mAnimating = false;
                });
            }
            else if(mState == State.Opened)
            {
                mPlayer = null;
            }
        }
    }


    public override void Dispatch(GameObject pObject)
    {
        if(mPlayer != null)
        {
            return;
        }
        if(mState == State.Opened)
        {
            return;
        }
        mPlayer = pObject.GetComponent<Player>();
        if(mPlayer == null)
        {
            return;
        }

        if(mPlayer.mActiveGate != null)
        {
            mPlayer = null;
            return;
        }
        UIManager.Instance().ShowNotification("Press A to Open Door");
        mTriggerable.DisableTriggerable();
        mPlayer.mActiveGate = this;

    }

    public override void ResetDispatch(GameObject pObject)
    {
        if(mPlayer == null)
        {
            return;
        }
        if(mState == State.Opened)
        {
            return;
        }
        if(mPlayer.gameObject != pObject)
        {
            return;
        }
        if(mPlayer.mMovementScript.GetState() == PlayerMovement.NavState.Paused)
        {
            return;
        }
        UIManager.Instance().HideNotification();
        mPlayer.mActiveGate = null;
        mPlayer = null;
    }

    public void RemoveTriggerable()
    {
        mTriggerable.DontActivateOnExit();
        mTriggerable.gameObject.SetActive(false);
    }

    public void OpenGate(Animator pAnimator, bool pKey)
    {
        if(mState != State.Closed)
        {
            return;
        }
        UIManager.Instance().HideNotification();
        if (!mUnlocked && !pKey)
        {
            AudioManager.PlaySFX("ClosedDoor");
            pAnimator.SetBool("Opened", false);
            pAnimator.SetTrigger("Door");
        }
        else if(mUnlocked || pKey)
        {
            AudioManager.PlaySFX("OpenDoor");
            pAnimator.SetBool("Opened", true);
            pAnimator.SetTrigger("Door");
            mState = State.Opening;
            RemoveTriggerable();
        }
    }
}
