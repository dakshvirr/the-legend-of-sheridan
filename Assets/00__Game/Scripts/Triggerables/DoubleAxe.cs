﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleAxe : Weapons
{
    public Transform mLeftHandHandle;
    public Transform mRightHandHandle;

    public override void FireWeapon()
    {

    }

    void Start()
    {
        mType = WeaponType.DoubleAxe;
    }

    void Update()
    {
        if (mActivePicker != null && mTriggerable.gameObject.activeInHierarchy)
        {
            if (mActivePicker.mAllowedWeapons[mType].mPickedUp)
            {
                mTriggerable.gameObject.SetActive(false);
            }
        }
        if(mActivePicker != null)
        {
            if(mActivePicker.mAllowedWeapons[mType].mState == WeaponState.PickedUp || 
                mActivePicker.mAllowedWeapons[mType].mState == WeaponState.Attack)
            {
                AnchorToHand();
            }
            else if(mActivePicker.mAllowedWeapons[mType].mState == WeaponState.Holster)
            {
                AnchorToHolster();
            }
        }
    }

    void AnchorToHand()
    {
        transform.position += mActivePicker.mAllowedWeapons[mType].mKh * (mActivePicker.mWeaponAnchor.position - transform.position);
        if(mActivePicker.mAllowedWeapons[mType].mState != WeaponState.PickedUp)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, mActivePicker.mWeaponAnchor.rotation, mActivePicker.mAllowedWeapons[mType].mKh);
        }
    }

    void AnchorToHolster()
    {
        transform.position += mActivePicker.mAllowedWeapons[mType].mKh * (mActivePicker.mHolsterAnchor.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, mActivePicker.mHolsterAnchor.rotation, mActivePicker.mAllowedWeapons[mType].mKh);
    }

    void OnTriggerEnter(Collider other)
    {
        if(mActivePicker == null)
        {
            return;
        }
        if (!(mActivePicker.mAllowedWeapons[mType].mState == WeaponState.Attack))
        {
            return;
        }
        if (other.attachedRigidbody == null)
        {
            Destructible aDestructible = other.gameObject.GetComponent<Destructible>();
            if(aDestructible == null)
            {
                return;
            }
            if(aDestructible.mDestroy)
            {
                return;
            }
            AudioManager.PlaySFX("DestroyDestructible");
            aDestructible.mDestroy = true;
            return;
        }

        if (other.attachedRigidbody.gameObject == mActivePicker.transform.parent.gameObject)
        {
            return;
        }

        Damageable aDamageable = other.attachedRigidbody.gameObject.GetComponent<Damageable>();
        if (aDamageable == null)
        {
            return;
        }
        aDamageable.Damage(mWeaponDamage, Damageable.DamagerType.WalkEnemy);
    }

}
